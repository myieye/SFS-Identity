﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SFS_Projekt_ASP.NET_Identity.Domain
{
  public class Task
  {
    public Guid Id { get; set; }

    public string Description { get; set; }

    public string ResponsibleUser { get; set; }

    public bool IsCompleted { get; set; }
  }
}