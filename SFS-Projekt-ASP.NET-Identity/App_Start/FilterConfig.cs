﻿using System.Web;
using System.Web.Mvc;

namespace SFS_Projekt_ASP.NET_Identity {
  public class FilterConfig {
    public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
      filters.Add(new HandleErrorAttribute());
    }
  }
}
