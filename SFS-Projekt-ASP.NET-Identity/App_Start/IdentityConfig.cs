﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using SFS_Projekt_ASP.NET_Identity.Models;

namespace SFS_Projekt_ASP.NET_Identity
{
  public class Claims
  {
    public const string TASKS_DELETE = "Tasks_Delete";

    public class Types
    {
      public const string PERMISSION = "Permission";
    }
    public class Roles
    {
      public const string ADMIN = "Admin";
    }
  }

  // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
  public class ApplicationUserManager : UserManager<ApplicationUser>
  {
    public ApplicationUserManager(IUserStore<ApplicationUser> store)
        : base(store)
    {

    }
    
    public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
    {
      var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()));
      
      var adminEmail = $"{Claims.Roles.ADMIN}@{Claims.Roles.ADMIN}.com";
      var adminPassword = "123456";

      // Init Admin User
      if (manager.FindByEmail(adminEmail) == null) {
        // Create the Admin user
        var admin = new ApplicationUser();
        admin.UserName = admin.Email = adminEmail;
        manager.Create(admin, adminPassword);

        // Add admin to admin role
        manager.AddToRole(admin.Id, Claims.Roles.ADMIN);
        
        // Add the Delete permission
        manager.AddClaim(admin.Id, new Claim(Claims.Types.PERMISSION, Claims.TASKS_DELETE));
      }

      // Configure validation logic for usernames
      manager.UserValidator = new UserValidator<ApplicationUser>(manager)
      {
        AllowOnlyAlphanumericUserNames = false,
        RequireUniqueEmail = true
      };

      // Configure validation logic for passwords
      manager.PasswordValidator = new PasswordValidator
      {
        RequiredLength = 6,
        RequireNonLetterOrDigit = true,
        RequireDigit = true,
        RequireLowercase = true,
        RequireUppercase = true,
      };

      // Configure user lockout defaults
      manager.UserLockoutEnabledByDefault = true;
      manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
      manager.MaxFailedAccessAttemptsBeforeLockout = 5;

      return manager;
    }
  }


  // Configure the application sign-in manager which is used in this application.
  public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
  {
    public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
        : base(userManager, authenticationManager)
    {
      
    }

    public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
    {
      return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
    }

    public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
    {
      return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
    }
  }

  public class ApplicationRoleManager : RoleManager<IdentityRole>
  {
    public ApplicationRoleManager(IRoleStore<IdentityRole, string> store)
      : base(store)
    {

    }

    internal static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
    {
      var roleManager = new ApplicationRoleManager(new RoleStore<IdentityRole>(context.Get<ApplicationDbContext>()));

      // Init Admin Role
      if (!roleManager.RoleExists(Claims.Roles.ADMIN))
      {
        roleManager.Create(new IdentityRole(Claims.Roles.ADMIN));
      }
      
      return roleManager;
    }
  }
}
