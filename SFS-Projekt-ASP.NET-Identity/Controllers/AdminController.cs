﻿using SFS_Projekt_ASP.NET_Identity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SFS_Projekt_ASP.NET_Identity.Controllers
{
  [Authorize(Roles = Claims.Roles.ADMIN)]
  public class AdminController : Controller
  {
    [HttpDelete]
    [ValidateAntiForgeryToken]
    public ActionResult DeleteTask(Guid id)
    {
      BusinessLogic.BLL.Logic.DeleteTask(id);

      return RedirectToAction("RefreshTaskList", "Tasks");
    }
    
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult CreateTask(string taskName)
    {
      BusinessLogic.BLL.Logic.AddTask(
        new Task()
        {
          Id = Guid.NewGuid(),
          Description = taskName,
          IsCompleted = false
        }
        );
      
      return RedirectToAction("RefreshTaskList", "Tasks");
    }
  }
}