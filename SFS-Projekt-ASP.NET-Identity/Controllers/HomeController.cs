﻿using System.Web.Mvc;

namespace SFS_Projekt_ASP.NET_Identity.Controllers
{
  public class HomeController : Controller
  {
    public ActionResult Index()
    {
      if (Request.IsAuthenticated)
      {
        return RedirectToAction("Tasks", "Tasks");
      }
      else
      {
        return View();
      }
    }
  }
}