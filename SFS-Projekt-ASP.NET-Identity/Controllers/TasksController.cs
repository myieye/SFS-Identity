﻿using SFS_Projekt_ASP.NET_Identity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SFS_Projekt_ASP.NET_Identity.Controllers
{
  public class TasksController : Controller
  {
    [Authorize]
    public ActionResult Tasks()
    {
      IEnumerable<Task> model = BusinessLogic.BLL.Logic.GetTasks();
      
      return View(model);
    }

    [Authorize]
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult AcceptTask(string id)
    {
      BusinessLogic.BLL.Logic.AcceptTask(Guid.Parse(id), User.Identity.Name);

      return RedirectToAction("RefreshTaskList");
    }

    [Authorize]
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult CompleteTask(string id)
    {
      Guid idAsGuid = Guid.Parse(id);
      Task task = BusinessLogic.BLL.Logic.GetTask(idAsGuid);
      if (string.IsNullOrEmpty(task.ResponsibleUser))
      {
        ModelState.AddModelError($"Complete-{id}", "This task isn't assigned to anyone.");
      }
      if (task.ResponsibleUser != User.Identity.Name)
      {
        ModelState.AddModelError($"Complete-{id}", "You are not responsible for this task.");
      }
      if (TempData.ContainsKey("CompleteTaskErrors"))
      {
        TempData.Remove("CompleteTaskErrors");
      }
      if (ModelState.IsValid)
      {
        BusinessLogic.BLL.Logic.CompleteTask(idAsGuid);
      } else
      {
        TempData["CompleteTaskErrors"] = ModelState;
      }

      return RedirectToAction("RefreshTaskList");
    }

    [Authorize]
    public ActionResult RefreshTaskList()
    {
      if (TempData.ContainsKey("CompleteTaskErrors"))
      {
        ModelState.Merge((ModelStateDictionary)TempData["CompleteTaskErrors"]);
      }
      
      IEnumerable<Task> model = BusinessLogic.BLL.Logic.GetTasks();

      return PartialView("_TaskList", model);
    }
  }
}