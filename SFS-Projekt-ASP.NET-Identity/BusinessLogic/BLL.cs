﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SFS_Projekt_ASP.NET_Identity.BusinessLogic
{
    public static class BLL
    {
        private static ILogic logic = new Logic();

        public static ILogic Logic
        {
            get { return logic; }
        }
    }
}