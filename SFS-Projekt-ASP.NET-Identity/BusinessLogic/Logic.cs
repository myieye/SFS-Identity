﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SFS_Projekt_ASP.NET_Identity.Domain;

namespace SFS_Projekt_ASP.NET_Identity.BusinessLogic
{
  public class Logic : ILogic
  {
    private ICollection<Task> tasks = new List<Task>()
    {
      new Task()
      {
        Id = Guid.NewGuid(),
        Description = "Write unit-tests",
        IsCompleted = false
      },
      new Task()
      {
        Id = Guid.NewGuid(),
        Description = "Prepare presentation",
        ResponsibleUser = "Ivan Ivanov",
        IsCompleted = true
      },
      new Task()
      {
        Id = Guid.NewGuid(),
        Description = "Create a UML diagram",
        ResponsibleUser = "Max Mustermann",
        IsCompleted = false
      },
      new Task()
      {
        Id = Guid.NewGuid(),
        Description = "Implement business logic methods",
        IsCompleted = false
      },
      new Task()
      {
        Id = Guid.NewGuid(),
        Description = "Create views",
        IsCompleted = false
      }
    };

    public void AcceptTask(Guid id, string userName)
    {
      if (id != Guid.Empty)
      {
        var task = tasks.SingleOrDefault(i => i.Id == id);
        task.ResponsibleUser = userName;
      }
    }

    public void AddTask(Task task)
    {
      if (task != null)
      {
        tasks.Add(task);
      }
    }

    public void CompleteTask(Guid id)
    {
      if (id != Guid.Empty)
      {
        var task = tasks.SingleOrDefault(i => i.Id == id);
        task.IsCompleted = true;
      }
    }

    public void DeleteTask(Guid id)
    {
      if (id != Guid.Empty)
      {
        var task = tasks.SingleOrDefault(i => i.Id == id);
        tasks.Remove(task);
      }
    }

    public Task GetTask(Guid id)
    {
      if (id != Guid.Empty)
      {
        return tasks.SingleOrDefault(i => i.Id == id);
      }
      return null;
    }

    public IEnumerable<Task> GetTasks()
    {
      return tasks;
    }
  }
}