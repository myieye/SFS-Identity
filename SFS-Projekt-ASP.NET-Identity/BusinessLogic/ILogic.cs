﻿using SFS_Projekt_ASP.NET_Identity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SFS_Projekt_ASP.NET_Identity.BusinessLogic
{
  public interface ILogic
  {
    IEnumerable<Task> GetTasks();

    void AddTask(Task task);

    void DeleteTask(Guid id);

    void CompleteTask(Guid id);

    void AcceptTask(Guid id, string userName);

    Task GetTask(Guid id);
  }
}