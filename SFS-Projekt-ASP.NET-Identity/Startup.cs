﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SFS_Projekt_ASP.NET_Identity.Startup))]
namespace SFS_Projekt_ASP.NET_Identity
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
